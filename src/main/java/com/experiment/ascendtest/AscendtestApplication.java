package com.experiment.ascendtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AscendtestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AscendtestApplication.class, args);
	}

}
