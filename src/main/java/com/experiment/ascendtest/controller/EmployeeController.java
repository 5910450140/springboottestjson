package com.experiment.ascendtest.controller;

import com.experiment.ascendtest.model.Employee;
import com.sun.xml.internal.ws.api.pipe.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class EmployeeController {
    List<Employee> employees = new ArrayList<>(Arrays.asList(
            new Employee("John", "male", "cleaner", 10000),
            new Employee("Lenon", "male", "manager", 15000)
    ));

    @Autowired
    ResourceLoader resourceLoader;


    @RequestMapping("/employee")
    public ResponseEntity readjson() throws IOException {
//        File file = new File("src/main/resources/data.json");
//        FileReader fileReader = new FileReader(file);
//        BufferedReader br = new BufferedReader(fileReader);
        URL url = resourceLoader.getClassLoader().getResource("data.json");
        File file = new File(url.getFile());
        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[(int) file.length()];
        fis.read(data);
        fis.close();

        String str = new String(data, "UTF-8");
        return ResponseEntity.status(HttpStatus.OK).header("Content-type", "application/json" ).body(str);


    }
//    public List<Employee> getAllEmployee(){
//        return employees;
//    }



}
