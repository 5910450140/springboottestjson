package com.experiment.ascendtest.model;

public class Employee {
    private String name;
    private String gender;
    private String roles;
    private int salary;

    public Employee(String name, String gender, String roles, int salary) {
        this.name = name;
        this.gender = gender;
        this.roles = roles;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}